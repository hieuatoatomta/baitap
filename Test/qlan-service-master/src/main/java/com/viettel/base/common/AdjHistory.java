package com.viettel.base.common;

/**
 * 
 * @author ToanBD
 *
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;  
  
@Retention(RetentionPolicy.RUNTIME)  // chú thích mức độ tồn tại của một annotation. RUNTIME thì mức độ cao nhất, được biên dịch và máy ảo thời điểm chạy cũng nhận ra sự tồn tại của nó
@Target({ElementType.METHOD, ElementType.FIELD}) // Dùng để chú thích cho một annotation khác, và các annotation đó được sử dung trong phạm vi nào
// https://o7planning.org/vi/10197/huong-dan-java-annotation

public @interface AdjHistory {
    // phần tử field có giá trị mặc định
	String field() default "";
}  
