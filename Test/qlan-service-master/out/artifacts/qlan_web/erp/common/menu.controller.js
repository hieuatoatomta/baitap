(function () {
    'use strict';

    let controllerId = 'MenuController';

    angular.module('MetronicApp').controller(controllerId, MenuController);

    // xử lý cái menu

    /* @ngInject */
    function MenuController($scope, $rootScope, Constant, $http, Restangular, CommonService) {
        let vm = this;
        $scope.Constant = Constant; // truy cap vào app.constants.js để nhận dữ liệu

        $scope.$watch
        (
            function () {
                return $rootScope.authenticatedUser;
            },
            function (casUser) {
                if (casUser == null) {
                    return;
                }
                console.log(casUser);
                vm.menuObjects = casUser.listObject;
                console.log(casUser.listObject);
            }
        )


        vm.goTo = goTo;

        /*
         * get menu text - neu vsa tra ve null thi
         */

        vm.getMenuText = function (obj) {
            try {
                let template = Constant.getTemplateUrl(obj.objectUrl);
                if (template == null) {
                    return obj.objectName + CommonService.translate("undeploy");
                }
                return template.title;
            } catch (err) {
                console.debug(err);
                return "N/A";
            }
        }

        /* Handle action client on a menu item */

        // trong mục menu nhận sự lựa chọn trong menu
        function goTo(menuKey) {
            debugger;
            let template = Constant.getTemplateUrl(menuKey);
            console.debug("postal", postal);
            postal.publish({
                channel: "Tab",
                topic: "open",
                data: template
            });
        }
    }
})();