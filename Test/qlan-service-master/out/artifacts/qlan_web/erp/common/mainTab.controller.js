(function() {
	'use strict';

	var controllerId = 'MainTabController';

	angular.module('MetronicApp').controller(controllerId, MainTabController);

	/* @ngInject */
	function MainTabController($scope, $rootScope, $timeout, Constant, $ocLazyLoad, gettextCatalog, CommonService) {
		let vm = this;
		vm.listFileLoadedPath='';

		/**
		 * Set rootScope for version
		 */	
		$scope.tabs = [];
		vm.addTab = addTab;
		vm.removeTab = removeTab;
		vm.scrlTabsApi = {};
		
		vm.addVersionToPathList = function(fileList){
			let list=[];
			if(fileList){
				if(version){
					for(let i=0;i<fileList.length;i++){
						let temp=vm.addVersionToPath(fileList[i]);
						if(vm.listFileLoadedPath.indexOf(temp)<0){
							vm.listFileLoadedPath=vm.listFileLoadedPath+"#%^"+temp;
							list.push(temp);	
						}
					}
				}
			}
			return list;
		}
		vm.addVersionToPath = function(file){
			if(file){
				if(version){				
					return file+"?tsVersion="+version; 
				}
			}
			return file;
		}
		
		postal.subscribe({
			channel  : "Tab",
			  topic    : "active",
			  callback:function(){
				  if($scope.tabs[0])
				  $scope.tabs[0].active=true;
			  }
		})
		
		// Subscribe opening tab channel
		postal.subscribe({
            channel  : "Tab",
            topic    : "open",
            callback : function(data, envelope) {
            	let listFileNeedReload=vm.addVersionToPathList(data.lazyLoadFiles) ;
            	if(listFileNeedReload.length>0){
	                $ocLazyLoad.load({
	                    name: 'MetronicApp',
	                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
	                    files: listFileNeedReload
	                }).then(function(){
	                	vm.addTab(createTabData({
	                        title: data.title,
	                        templateUrl: vm.addVersionToPath(data.templateUrl),
	                        data: {
	                                id: 0
	                        },
	                        isSave: false,
	                        closable: true
	                	}));
	                });
            	}else{
            		vm.addTab(createTabData({
                        title: data.title,
                        templateUrl: vm.addVersionToPath(data.templateUrl),
                        data: {
                                id: 0
                        },
                        isSave: false,
                        closable: true
                	}));
            	}
            }
		});

		function getTab(title, templateUrl, data){
            for(let i = 0; i < $scope.tabs.length; i ++){
                if(data === undefined || data.id === undefined || $scope.tabs[i].data.id === undefined){
                    if($scope.tabs[i].templateUrl == templateUrl){
                            return i;
                    }
                }else{
                    if($scope.tabs[i].templateUrl == templateUrl && $scope.tabs[i].data.id == data.id){
                            return i;
                    }
                }
            }

            return -1;
		}

		/* Add new tab */
		function addTab(tabInfo) {
            //Check duplicate tabs
            let index = getTab(tabInfo.title, tabInfo.templateUrl, tabInfo.data);

            if (index < 0) {
                $timeout(function(){
                        $scope.tabs.push(tabInfo);
                        $scope.tabs[$scope.tabs.length - 1].active = true;
                },0);
            }else{
                $timeout(function(){
                		//close old tab
                		$scope.tabs.splice(index, 1);
                		$scope.tabs.push(tabInfo);
 	                    $scope.tabs[$scope.tabs.length - 1].active = true;
                },0);
            }

            scrollToView();


            $(document).find(".font-red-sunglo").removeClass("font-red-sunglo")
		}

		// Remove tab by index
		function removeTab (event, index) {
            event.preventDefault();
            event.stopPropagation();

            //Unset templateUrl out of tabNames array
            let templateUrl = $scope.tabs[index].templateUrl;
            let id = $scope.tabs[index].data.id;
            //delete $scope.tabNames[templateUrl];

            $scope.tabs.splice(index, 1);

            //settingService.closeWorkingTab(templateUrl, id);
		};

		function removeTabByTemplate (templateUrl, id) {
            for(var i = 0; i < $scope.tabs.length; i ++){
                if($scope.tabs[i].templateUrl == templateUrl){
                    $scope.tabs.splice(i, 1);
                    break;
                }
            }
		};

		function createTabData(options){
	        function TabData(options){
                if (typeof options === 'object') {
                    for ( let key in options) {
                        this[key] = options[key];
                    }
                }
	        }
	
	        TabData.prototype.getId = function(){
                return this.data.id;
	        };
	
	        var tabData = new TabData(options);
	        return tabData;
		};

		
		function scrollToView() {
			if (vm.scrlTabsApi.scrollTabIntoView) {
				vm.scrlTabsApi.scrollTabIntoView();
			}
		};
		
		$scope.$on('tab-close', function(event, args) {
			//console.log(args.template.templateUrl);
			
			/*if (_.find($scope.tabs, function(item){return item.templateUrl = args.template.templateUrl})) {
				console.log("Closing tab..");
				removeTabByTemplate(args.template.templateUrl, 0);
			}*/
		    // do what you want to do
		});
	}
})();