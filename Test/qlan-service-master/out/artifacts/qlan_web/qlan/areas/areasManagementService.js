angular.module('MetronicApp').factory('areasManagementService',
    ['$http', '$q', 'RestEndpoint', 'Restangular',

        function ($http, $q, RestEndpoint, Restangular) {
            let serviceUrl = RestEndpoint.AREA_URL;
            return {
                doSearch: doSearch,
                doSearchTTT: doSearchTTT,
                doSearchTree: doSearchTree,
                remove: remove,
                updateAreas: updateAreas,
                addAreas: addAreas
            };

            function doSearch(obj) {
                return Restangular.all(serviceUrl + "/doSearch").post(obj);
            }

            function doSearchTTT(obj) {
                return Restangular.all(serviceUrl + "/doSearchTT").post(obj);
            }

            function doSearchTree() {
                return Restangular.all(serviceUrl + "/doSearchTree").post();
            }

            function remove(obj) {
                return Restangular.all(serviceUrl + "/remove").post(obj);
            }

            function updateAreas(obj) {
                return Restangular.all(serviceUrl + "/update").post(obj);
            }

            function addAreas(obj) {
                return Restangular.all(serviceUrl + "/add").post(obj);
            }

        }]);
